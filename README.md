# artificial-intelligence

Eine wichtige Aufgabe von Artificial Intelligence in Games ist es, die Illusion von intelligentem Verhalten der Charaktere zu erzeugen.
Eine einfache und sehr häufige Herangehensweise dies für einfachere Verhaltensweisen zu implementieren sind State Machines.
Wir haben im Modul State Machines schon FSM und das State Pattern kennengelernt.
Als kleine Challenge für heute würde ich euch bitten, ein NPC-Verhalten mit Hilfe dieses Patterns zu implementieren.
Im Buch Programming Game AI by Example von Mat Buckland wird ein Goldgräber im Wilden Westen auf diese Art und Weise simuliert. Das Bild zeigt seine States und Transitions. 

![State Diagram](MinerStateMachine.png)

Als Ergebnis/Feedback werden im Buch lediglich ein Texte in der Konsole ausgegeben. 
"Pickin' up a nugget"
"Pickin' up a nugget"
"Ah'm leavin' the gold mine with mah pockets full o' sweet gold"
"Goin' to the Bank. Yes siree"
"Depositin' gold. Total savings now: 4"
"Leavin' the bank"
"Walkin' to the gold mine"
etc.

Ihr könnt natürlich ein ähnliches Verhalten für einen anderen Character wählen, der besser zu unserem Dungeon Crawler passt.
Und es bietet sich auch an, Bewegung und Animationen in Unity mit den States mitlaufen zu lassen. 
In diesem Zusammenhang wäre es auch eine interessante Möglichkeit, einen Layer des Animators (z.B. über State Machine Behaviours) als eigentliche "State Engine" zu benutzen und alles von dort aus zu steuern.
